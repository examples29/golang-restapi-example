# Example of a REST API based in the official tutorial https://go.dev/doc/tutorial/web-service-gin

## Prerequisites

https://go.dev/doc/install

## How to use:

Run with:

``go run .``

GET all items:

``curl http://localhost:8080/albums``

POST a new item:

``curl http://localhost:8080/albums \
    --include \
    --header "Content-Type: application/json" \
    --request "POST" \
    --data '{"id": "4","title": "The Modern Sound of Betty Carter","artist": "Betty Carter","price": 49.99}'``

GET an item by id:

``curl http://localhost:8080/albums/1``
